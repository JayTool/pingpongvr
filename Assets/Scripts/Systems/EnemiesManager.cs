using System;
using System.Threading;
using PingPong.Core;
using UnityEngine;
using UnityEngine.Assertions;
using Utils.Maps;
using WeirdFactory.Utils;

namespace PingPong.Enemies
{
    public class EnemiesManager
    {
        private CtxContainer _ctxContainer;

        public EnemiesManager (CtxContainer ctxContainer)
        {
            _ctxContainer = ctxContainer;
        }

        public void InstantiateEnemy(EnemyType enemyType, Vector3 spawnPos, Quaternion rot,
            IScope scope, CancellationToken ct)
        {
            if (enemyType == EnemyType.CannonballShooter)
            {
                var shooterPrefab =
                    _ctxContainer.MapsContainer.enemiesPrefabsMap.EnemiesPrefabs[EnemyType.CannonballShooter];
                var shooterGo = GameObject.Instantiate(shooterPrefab, spawnPos, rot);
                var shooterView = shooterGo.GetComponent<CannonballEnemyView>();
                Assert.IsNotNull(shooterView);
                var shooter = new CannonballEnemy(shooterView, _ctxContainer, ct);
                
                shooter.Run(scope);
            }
            else if (enemyType == EnemyType.BlueShooter)
            {
                var shooterPrefab =
                    _ctxContainer.MapsContainer.enemiesPrefabsMap.EnemiesPrefabs[EnemyType.BlueShooter];
                var shooterGo = GameObject.Instantiate(shooterPrefab, spawnPos, rot);
                var shooterView = shooterGo.GetComponent<BlueEnemyView>();
                Assert.IsNotNull(shooterView);
                var shooter = new BlueEnemy(shooterView, _ctxContainer, ct);
                
                shooter.Run(scope);
            }
            else if (enemyType == EnemyType.RedShooter)
            {
                var shooterPrefab =
                    _ctxContainer.MapsContainer.enemiesPrefabsMap.EnemiesPrefabs[EnemyType.RedShooter];
                var shooterGo = GameObject.Instantiate(shooterPrefab, spawnPos, rot);
                var shooterView = shooterGo.GetComponent<RedEnemyView>();
                Assert.IsNotNull(shooterView);
                var shooter = new RedEnemy(shooterView, _ctxContainer, ct);
                
                shooter.Run(scope);
            }
            else if (enemyType == EnemyType.RedAngryWalle)
            {
                var enemyPrefab =
                    _ctxContainer.MapsContainer.enemiesPrefabsMap.EnemiesPrefabs[EnemyType.RedAngryWalle];
                var enemyGo = GameObject.Instantiate(enemyPrefab, spawnPos, rot);
                var enemyView = enemyGo.GetComponent<AngryWalleView>();
                Assert.IsNotNull(enemyView);
                var enemy = new AngryWalleEnemy(enemyView, AngryWalleEnemy.AngryWalleWariant.Red,
                    ProjectileTypes.Red, _ctxContainer, ct);
                
                enemy.Run(scope);
            }
            else
            {
                Debug.LogError($"There is no shooter type {enemyType}");
                return;
            }
        }
    }
}