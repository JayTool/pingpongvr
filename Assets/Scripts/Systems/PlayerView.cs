using UnityEngine;

namespace PingPong.Views
{
    public class PlayerView : MonoBehaviour
    {
        public HandView RightHandView;
        public HandView LeftHandView;
        public Rigidbody PlayerRb;
        public Transform HeadPointTf;
        public GameObject StrongStrikeFx;
    }
}