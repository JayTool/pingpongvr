using PingPong.Systems;
using PingPong.Views;
using UnityEngine;

namespace PingPong.Core
{
    [RequireComponent(typeof(Animator))]
    public class HandsAnimatorInitiator : MonoBehaviour
    {
        private void Start()
        {
            var animator = GetComponent<Animator>();
            transform.parent.parent.GetComponent<HandView>().Animator = animator;
        }
    }
}