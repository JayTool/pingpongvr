using System.Threading;
using System.Threading.Tasks;
using PingPong.Core;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using WeirdFactory.Utils;

namespace PingPong.Weapons
{
    public class StationaryWeapon
    {
        protected StationaryWeaponView WeaponView;
        private CtxContainer _ctxContainer;

        private bool _loadedPlaceOccupied; // If cannon isn't loaded, and we brought a projectile to load place
        private bool _loaded;

        public StationaryWeapon(StationaryWeaponView weaponView, CtxContainer ctxContainer)
        {
            WeaponView = weaponView;
            _ctxContainer = ctxContainer;
        }

        public async void Run(IScope scope, CancellationToken ct)
        {
            using (scope.SubScope(out var innerScope))
            {
                var innerCt = new CancellationTokenSource();
                innerScope.Subscribe(Clean);

                WeaponView.LoadColObs.OnTriggerEnterEvt += OnTriggerEnterHandler;
                WeaponView.LoadColObs.OnTriggerExitEvt += OnTriggerExitHandler;

                var manipulatorsSystem = _ctxContainer.Starter.ManipulatorsSystem;
                manipulatorsSystem.RightOnInteractableThrow += TryToLoad;
                
//                _ctxContainer.Starter.ManipulatorsSystem._rightControllerInput.GripBtnPress
                
                while (true)
                {
                    if (ct.IsCancellationRequested)
                        break;
                    
                    Flow();
                    await Task.Yield();
                }

                void Clean()
                {
                    innerCt.Cancel();
                    WeaponView.LoadColObs.OnTriggerEnterEvt -= OnTriggerEnterHandler;
                    WeaponView.LoadColObs.OnTriggerExitEvt -= OnTriggerExitHandler;
                }
            }
        }

        private void OnTriggerEnterHandler(Collider col)
        {
            CheckReadyToLoad(col, true);
        }
        
        private void OnTriggerExitHandler(Collider col)
        {
            CheckReadyToLoad(col, false);
        }

        private void CheckReadyToLoad(Collider col, bool isEnter)
        {
            var go = col.gameObject;
            var layer = go.layer;

            if (layer == Layers.ProjectileLayer)
            {
                _loadedPlaceOccupied = isEnter;
                Debug.Log($"_allowedToLoad = {_loadedPlaceOccupied}");
            }
        }

        private void TryToLoad(XRBaseInteractable interactable)
        {
            //Debug.Log($"Try to load - {interactable.gameObject}");
            if (interactable.gameObject.layer == Layers.ProjectileLayer)
            {
                var projView = interactable.GetComponent<ProjectileView>();
                projView.Collider.enabled = false;
                projView.Rigidbody.isKinematic = true;

                projView.transform.position = WeaponView.LoadPlace.position;
            }

            Debug.Log($"Try to load");
            //if ()
        }

        protected virtual void Flow()
        {
        }
    }
}