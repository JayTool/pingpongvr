using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PingPong.Core;
using UnityEngine;
using WeirdFactory.Utils;

namespace PingPong.Enemies
{
    //TODO Refactor
    public class ProjectileManager
    {
        public Pools Pools;
        
        private CtxContainer _ctxContainer;
        private List<Projectile> _activeProjectiles;
        private const float STRONG_STRIKE_VELOCITY = 1f;

        public ProjectileManager (CtxContainer ctxContainer)
        {
            _ctxContainer = ctxContainer;
            _activeProjectiles = new List<Projectile>();
            Pools = new Pools(ctxContainer);
        }
        
        public async void Run(IScope scope, CancellationToken ct)
        {
            using (scope.SubScope(out var innerScope))
            {
                var innerCt = new CancellationTokenSource();
                innerScope.Subscribe(Clean);

                while (true)
                {
                    if (ct.IsCancellationRequested)
                        break;
                    await Task.Yield();
                    
                    foreach (var projectile in _activeProjectiles)
                    {
                        if (!projectile.ProjectileView.gameObject.activeSelf)
                        {
                            _activeProjectiles.Remove(projectile);
                            break;
                        }

                        if (projectile.ProjectileView.Rigidbody.isKinematic)
                        {
                            var velocityDir = projectile.KinematicDirection;
                            projectile.ProjectileView.transform.Translate(velocityDir * STRONG_STRIKE_VELOCITY, Space.World);
                            HitAimCheck(projectile);
                        }
                    }
                }

                void Clean()
                {
                    innerCt.Cancel();
                }
            }

        }

        public void AddProjectile(Projectile proj)
        {
            _activeProjectiles.Add(proj);
        }

        public Projectile GetProjectileByView(ProjectileView view)
        {
            foreach (var projectile in _activeProjectiles)
            {
                if (projectile.ProjectileView == view)
                    return projectile;
            }
            
            Debug.LogError("Projectile not found");
            return null;
        }

        private void HitAimCheck(Projectile projectile)
        {
            var projPos = projectile.ProjectileView.transform.position;
            var aimList = _ctxContainer.Starter.AimsManager.AimViews;
            
//            foreach (var aim in aimList)
//            {
                //https://docs.unity3d.com/ScriptReference/Physics.SphereCast.html
                if (projectile.ProjectileView.Rigidbody.isKinematic)
                {
                    var ray = new Ray(projPos, projectile.KinematicDirection);
                    var layerMask = Layers.AimMask;
                    Physics.SphereCast(ray, 1f, out var hitInfo,2f, layerMask);
                    if (hitInfo.transform != null)
                    {
                        var aimView = hitInfo.transform.gameObject.GetComponent<AimView>();
                        _ctxContainer.Starter.AimsManager.HitAim(aimView);
                    }
                }

                //var dir = projectile.KinematicDirection
                //var ray = new Ray(projPos,);
//            }
        }
    }
}