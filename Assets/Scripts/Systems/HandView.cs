using PingPong.Core;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PingPong.Views
{
    public class HandView : MonoBehaviour
    {
        public Collider Collider;
        public CollisionsSphereCastObserver colSphereCastObserver;
        public float Velocity;
        public Transform InsideNormal;
        public Transform OutsideNormal;
        public XRDirectInteractor XrDirectInteractor;
        public Vector3 PrevPos;
        public Animator Animator;
    }
}