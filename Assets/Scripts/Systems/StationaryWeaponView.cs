using PingPong.Core;
using UnityEngine;

namespace PingPong.Weapons
{
    public class StationaryWeaponView : MonoBehaviour
    {
        public Transform ShotPointTf;
        public CollisionsObserver LoadColObs;
        public Transform LoadPlace;
    }
}