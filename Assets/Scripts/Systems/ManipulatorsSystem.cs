using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using PingPong.Core;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using WeirdFactory.Utils;
using WeirdFactory.Utils.Copticool.Utils;

namespace PingPong.Systems
{
    public class ManipulatorsSystem
    {
        //private SimpleGOPool _strongStrikePoolFx;
        //private const float STRONG_STRIKE_SENSIVITY = 0f;
        public event Action<XRBaseInteractable> RightOnInteractableTake; 
        public event Action<XRBaseInteractable> RightOnInteractableThrow; 

        public XRBaseInteractable LeftTakenInteractable => _leftTakenInteractable;
        public XRBaseInteractable RightTakenInteractable => _rightTakenInteractable;

        private CtxContainer _ctxContainer;
        private XRBaseInteractable _leftTakenInteractable;
        private XRBaseInteractable _rightTakenInteractable;
        
        private ControllerInput _rightControllerInput;
        private ControllerInput _leftControllerInput;

        private HandAnimationSystem _HandAnimationSystem;

        public ManipulatorsSystem(CtxContainer ctxContainer)
        {
            _ctxContainer = ctxContainer;
            //_strongStrikePoolFx = new SimpleGOPool(_ctxContainer.PlayerView.StrongStrikeFx);
        }

        public async void Run(IScope scope, CancellationToken ct)
        {
            var innerCt = new CancellationTokenSource();
            _rightControllerInput = new ControllerInput(InputDeviceCharacteristics.Controller |
                                                        InputDeviceCharacteristics.Right);
            _leftControllerInput = new ControllerInput(InputDeviceCharacteristics.Controller |
                                                       InputDeviceCharacteristics.Left);

            //TODO Left side
            var interactorRight = _ctxContainer.PlayerView.RightHandView.XrDirectInteractor;
            
            interactorRight.onSelectEntered.AddListener((interactable) =>
            {
                HandlerItemInteractableEnter(interactable, InputDeviceCharacteristics.Right); 
            });
            
            interactorRight.onSelectExited.AddListener((interactable) =>
            {
                //TODO It calls on exit application. Need to dispose this correctly
                HandlerItemInteractableExit(interactable, InputDeviceCharacteristics.Right); 
            });

            using (scope.SubScope(out var innerScope))
            {
                innerScope.Subscribe(Clean);

                var rightHandView = _ctxContainer.PlayerView.RightHandView;
                var leftHandView = _ctxContainer.PlayerView.LeftHandView;
                
                var rightHandAnim = new HandAnimationSystem(_rightControllerInput ,rightHandView);
                //var leftHandAnim = new HandAnimationSystem(_leftControllerInput ,leftHandView);
                
                _rightControllerInput.Run(innerScope, innerCt.Token);
                rightHandAnim.Run(innerScope, innerCt.Token);
                
                //leftHandAnim.Run(innerScope, innerCt.Token);

                while (!ct.IsCancellationRequested)
                {
                    await Task.Yield();
                }
            }

            void Clean()
            {
                innerCt.Cancel();
                interactorRight.onSelectEntered.RemoveAllListeners();
                interactorRight.onSelectExited.RemoveAllListeners();
                RightOnInteractableTake = null;
                RightOnInteractableThrow = null;
            }

            #region PingPong

#if PING_PONG
            void BounceLogic(HandView handView, Transform collidedTf)
            {
                var velocity = handView.Velocity;
                var testForce = - collidedTf.forward * velocity / 10f ;
                var insideNormal = handView.InsideNormal;
                var outsideNormal = handView.OutsideNormal;

                var normal = insideNormal.forward;
                /*
                var contactNormal = - collidedTf.forward;
                
                var isInside = (Math.Abs(contactNormal.x - normal.x) < 0.2f &&
                    Math.Abs(contactNormal.y - normal.y) < 0.2f && Math.Abs(contactNormal.z - normal.z) < 0.2f);
                */
                if (/*isInside &&*/ velocity > STRONG_STRIKE_SENSIVITY)
                {
                    var projectileView = collidedTf.gameObject.GetComponent<ProjectileView>();
                    var projectile = _ctxContainer.Starter.ProjectileManager.GetProjectileByView(projectileView);
                    projectile.KinematicDirection = normal;

                    projectileView.transform.forward = projectile.KinematicDirection;
                    projectile.ProjectileView.Rigidbody.isKinematic = false;
                    projectile.ProjectileView.Rigidbody.useGravity = true;
                    projectile.ProjectileView.Rigidbody.velocity = insideNormal.forward * 30f;
                    projectile.SourceEnemyTf = _ctxContainer.PlayerView.transform;
                    //projectile.ProjectileView.Rigidbody.AddForce(testForce * 3f, ForceMode.Impulse);

                    var fx = _strongStrikePoolFx.GetOrAdd();
                    fx.transform.position = handView.transform.position + normal * 1f;
                    fx.SetActive(true);
                }
                else
                {
                    var rb = collidedTf.GetComponent<Rigidbody>();
                    rb.useGravity = true;
                    rb.AddForce(testForce, ForceMode.Impulse);
                }
            }
#endif

            #endregion
        }
        
        private void HandlerItemInteractableEnter(XRBaseInteractable interactable, InputDeviceCharacteristics side)
        {
            if (side == InputDeviceCharacteristics.Right)
            {
                _rightTakenInteractable = interactable;
                RightOnInteractableTake?.Invoke(interactable);
            }
        }
        
        private void HandlerItemInteractableExit(XRBaseInteractable interactable, InputDeviceCharacteristics side)
        {
            if (side == InputDeviceCharacteristics.Right)
            {
                _rightTakenInteractable = null;
                RightOnInteractableThrow?.Invoke(interactable);
            }
        }
    }
}