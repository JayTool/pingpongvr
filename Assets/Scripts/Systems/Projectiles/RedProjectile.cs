using PingPong.Core;

namespace PingPong.Enemies
{
    public class RedProjectile : Projectile
    {
        private RedProjectileView _view;
        public const ProjectileTypes PROJECTILE_TYPE = ProjectileTypes.Red;

        public override ProjectileView ProjectileView
        {
            get => _view;
            set => _view = (RedProjectileView) value;
        }

        public override ProjectileTypes ProjectileType { get => ProjectileType; }
    }
}