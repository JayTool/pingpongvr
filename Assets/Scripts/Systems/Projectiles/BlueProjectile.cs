using PingPong.Core;

namespace PingPong.Enemies
{
    public class BlueProjectile : Projectile
    {
        private BlueProjectileView _view;
        public const ProjectileTypes PROJECTILE_TYPE = ProjectileTypes.Red;

        public override ProjectileView ProjectileView
        {
            get => _view;
            set => _view = (BlueProjectileView) value;
        }
        
        public override ProjectileTypes ProjectileType { get => ProjectileType; }

    }
}