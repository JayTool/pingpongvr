using PingPong.Core;
using UnityEngine;

namespace PingPong.Enemies
{
    public abstract class Projectile
    {
        public abstract ProjectileView ProjectileView { get; set; }
        public abstract ProjectileTypes ProjectileType { get; }
        public Vector3 KinematicDirection;

        public Transform SourceEnemyTf;
    }
}