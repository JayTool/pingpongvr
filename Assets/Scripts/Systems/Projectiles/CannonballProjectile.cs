using PingPong.Core;

namespace PingPong.Enemies
{
    public class CannonballProjectile : Projectile
    {
        private CannonballProjectileView _view;
        public const ProjectileTypes PROJECTILE_TYPE = ProjectileTypes.Red;


        public override ProjectileView ProjectileView
        {
            get => _view;
            set => _view = (CannonballProjectileView) value;
        }
        
        public override ProjectileTypes ProjectileType { get => ProjectileType; }
    }
}