using System;
using System.Collections;
using System.Threading;
using PingPong.Core;
using PingPong.Utils;
using States.BlueShooter;
using UnityEngine;

namespace PingPong.Enemies
{
    public class BlueEnemy : Enemy
    {
        private StateMachine _sm;
        
        public const float BODY_HORIZONTAL_THRESHOLD = 3f;
        public const float SHOOTER_VERTICAL_THRESHOLD = 5f;
        public const float BODY_HORIZONTAL_SPEED = 1f;
        public const float BODY_VERTICAL_SPEED = 1f;
        public const float TARGET_STOP_DISTANCE = 5f;
        public const float MOVING_SPEED = 0.1f;
        public const float SHOT_RECHARGE_TIME = 1f;

        public AimAndShootState_BlueShooter AimAndShootState;
        
        public BlueEnemy(BlueEnemyView enemyView, CtxContainer ctxContainer, CancellationToken ct) :
            base(ctxContainer, ProjectileTypes.Blue, ct)
        {
            EnemyView = enemyView;
            HP = 100;
            InitStates();
        }

        private void InitStates()
        {
            AimAndShootState = new AimAndShootState_BlueShooter(this);
            
            _sm = new StateMachine(AimAndShootState);
        }

        protected override void Flow()
        {
            _sm.Update();
        }
    }
}