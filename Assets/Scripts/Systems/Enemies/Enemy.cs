using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PingPong.Core;
using UnityEngine;
using WeirdFactory.Utils;
using WeirdFactory.Utils.Copticool.Utils;

namespace PingPong.Enemies
{
    public abstract class Enemy
    {
        public EnemyView EnemyView;
        public readonly CtxContainer CtxContainer;

        //TODO Move to enemyData struct
        public float CooldownTime;
        public float RechargeTime;
        public int ClipSize;
        public int AmmoCounter { get; protected set; }
        public float TimerToShot { get; protected set; }
        public bool IsCooldowning { get; protected set; }
        public bool IsRecharging { get; protected set; }
        public ProjectileTypes ProjectileTypes { get; protected set; }

        protected SimpleGOPool HitAimPoolFx;
        protected int HP;
        protected CancellationToken _ct;

        private List<Projectile> _projectilesList;

        protected Enemy(CtxContainer ctxContainer, ProjectileTypes projectileTypes, CancellationToken ct)
        {
            _projectilesList = new List<Projectile>();
            CtxContainer = ctxContainer;
            ProjectileTypes = projectileTypes;
            _ct = ct;

            //TODO Move to enemyData struct
            CooldownTime = 2f;
            RechargeTime = 5f;
            ClipSize = 100;
            AmmoCounter = ClipSize;
        }

        public async void Run(IScope scope)
        {
            using (scope.SubScope(out var innerScope))
            {
                innerScope.Subscribe(Clean);
                
                EnemyView.CollisionsObserver.OnCollisionEnterEvt += TakeDamage;
                while (true)
                {
                    if (_ct.IsCancellationRequested)
                        break;
                    
                    if (HP <= 0)
                        break;
                    
                    Flow();
                    ShotTimerProcess();
                    await Task.Yield();
                }

                void Clean()
                {
                    EnemyView.CollisionsObserver.OnCollisionEnterEvt -= TakeDamage;
                }
            }
        }

        protected abstract void Flow();

        private void TakeDamage(Collision col)
        {
            var layer = 1 << col.gameObject.layer;
            if (layer == Layers.ProjectileMask)
            {
                var projectileView = col.gameObject.GetComponent<ProjectileView>();
                var projectile = CtxContainer.Starter.ProjectileManager.GetProjectileByView(projectileView);
                
                if (projectile.SourceEnemyTf == EnemyView.transform)
                    return;
                
                var projHitFlags = projectileView.HitFlags;
                var shooterHitFlags = EnemyView.HitFlagsList;
                bool nonContains = false;

                foreach (var flag in shooterHitFlags)
                {
                    if (!projHitFlags.Contains(flag))
                    {
                        nonContains = true;
                        break;
                    }
                }

                if (nonContains)
                {
                    Debug.Log("Projectile doesn't fit to this target");
                    return;
                }

                HP -= 100;
                Debug.Log($"ProjectileHit. Hp = {HP}");
            }

            if (HP <= 0)
            {
                EnemyView.gameObject.SetActive(false);
            }
        }

        /*
        protected virtual void ShotHandler()
        {
            var shotPoints = EnemyView.ShotPoints;
            foreach (var point in shotPoints)
            {
                Shot(point, 2f);
            }
        }


*/
        protected virtual void UpdateTimerAfterShot()
        {
            if (AmmoCounter > 0)
            {
                TimerToShot = CooldownTime;
                IsCooldowning = true;
            }
            else
            {
                TimerToShot = RechargeTime;
                IsRecharging = true;
            }
        }

        private void ShotTimerProcess()
        {
            if (TimerToShot >= 0)
            {
                TimerToShot -= Time.deltaTime;
            }
            else
            {
                if (IsRecharging)
                {
                    TimerToShot = CooldownTime;
                    AmmoCounter = ClipSize;
                }

                IsRecharging = false;
                IsCooldowning = false;
            }
        }
    }
}