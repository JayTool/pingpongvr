using System.Collections;
using System.Collections.Generic;
using System.Threading;
using PingPong.Core;
using PingPong.Systems;
using PingPong.Utils;
using UnityEngine;
using WeirdFactory.Utils;

namespace PingPong.Enemies
{
    public class CannonballEnemy : Enemy
    {
        public CannonballEnemy(CannonballEnemyView enemyView, CtxContainer ctxContainer, CancellationToken ct) :
            base(ctxContainer, ProjectileTypes.Cannonball, ct)
        {
            EnemyView = enemyView;
            HP = 100;
        }
        protected override void Flow()
        {
            
        }
    }
}