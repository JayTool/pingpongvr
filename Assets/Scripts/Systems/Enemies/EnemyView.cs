using System.Collections.Generic;
using PingPong.Core;
using UnityEngine;

namespace PingPong.Enemies
{
    public class EnemyView : MonoBehaviour
    {
        public Rigidbody Rigidbody;
        public List<Transform> ShotPoints;
        public GameObject ProjectilePrefab;
        public CollisionsObserver CollisionsObserver;
        public List<ProjectileTypes> HitFlagsList;

        //public AimView AimView;
    }
}