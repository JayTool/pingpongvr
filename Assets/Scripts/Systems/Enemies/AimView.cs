using System.Collections.Generic;
using PingPong.Core;
using UnityEngine;

namespace PingPong.Enemies
{
    public class AimView : MonoBehaviour
    {
        //public CollisionsSphereCastObserver collisionsSphereCastObserver;
        public CollisionsObserver CollisionsObserver;
        public List<ProjectileTypes> HitFlagsList;
    }
}