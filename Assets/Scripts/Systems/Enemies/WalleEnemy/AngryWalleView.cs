using System.Collections.Generic;
using UnityEngine;

namespace PingPong.Enemies
{
    public class AngryWalleView : EnemyView
    {
        public List<Transform> CannonsTfList;
        public Transform BodyTf;
    }
}