using System;
using System.Threading;
using System.Threading.Tasks;
using PingPong.Core;
using PingPong.Utils;
using States.AngryWalle;
using UnityEngine;
using WeirdFactory.Utils;
using Random = UnityEngine.Random;

namespace PingPong.Enemies
{
    public class AngryWalleEnemy : Enemy
    {
        public AngryWalleView WalleView;

        public float MaxForwardSpeed { get; private set;} = 5f;
        public float RotationSpeed { get; private set;} = 0.1f;

        private float _changePosTime;
        private AngryWalleWariant _walleWariant;
        private StateMachine _sm;
        private Vector3 _targetPosition;

        // States
        private AngryWalle_MovingState _movingState;
        private AngryWalle_AimingState _aimingState;
        private AngryWalle_FiringState _firingState;

            
        public AngryWalleEnemy(AngryWalleView enemyView, AngryWalleWariant war, ProjectileTypes projType,
            CtxContainer ctxContainer, CancellationToken ct) 
            : base(ctxContainer, projType, ct)
        {
            _walleWariant = war;
            EnemyView = enemyView;
            WalleView = enemyView;
            HP = 200;
            CooldownTime = 0.5f; // TODO Set params in constructor

            InitStates();
        }

        private void InitStates()
        {
            var targetVec = new Vector3(10f, 0f, 10f);
            CtxContainer.GizmosPoints.Add(targetVec);
            _movingState = new AngryWalle_MovingState(this, targetVec);
            _movingState.OnTargetReached += OnTargetReached;

            _aimingState = new AngryWalle_AimingState(this);
            _aimingState.OnAimedChange += OnAimedChange;
            
            _firingState = new AngryWalle_FiringState(this);
            _firingState.SingleShotAct += OnSingleShot;
            //_firingState.ShotAct += ShotHandler;
            
            _sm = new StateMachine(_movingState);
            _sm.AddState(_aimingState);
            //_sm.AddState(_firingState);
            
            
        }

        private void OnTargetReached()
        {
            var playerTf = CtxContainer.PlayerView.transform;

            _sm.TryRemoveState(_movingState);
            _aimingState.SetTargetTf(playerTf);
            ChangeTargetPosPeriod();
        }

        async void ChangeTargetPosPeriod()
        {
            await AuxiliaryFunctions.TimerDelay(50f, _ct);

            var xRand = Random.Range(10f, 30f);
            var zRand = Random.Range(10f, 30f);
            var targetVec = new Vector3(xRand, 0f, zRand);

            _movingState.SetNewTarget(targetVec);
            _sm.AddState(_movingState);
            _aimingState.RemoveTargetTf();
        }

        protected override void Flow()
        {
            _sm.Update();
        }

        private void SetShotTimer(float time)
        {
            TimerToShot = time;
        }

        private void OnSingleShot()
        {
            AmmoCounter--;
            UpdateTimerAfterShot();
        }

        private void OnAimedChange(bool isAimed)
        {
            if (_aimingState.TargetTf != null)
            {
                if (isAimed)
                    _sm.AddState(_firingState);
                else
                    _sm.TryRemoveState(_firingState);
            }
            else
            {
                _sm.TryRemoveState(_firingState);
            }
        }

        public enum AngryWalleWariant
        {
            Red,
            Blue
        }
    }
}