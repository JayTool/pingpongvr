namespace PingPong.Enemies
{
    public enum EnemyType
    {
        CannonballShooter,
        RedShooter,
        BlueShooter,
        RedAngryWalle,
        BlueAngryWalle
    }
}