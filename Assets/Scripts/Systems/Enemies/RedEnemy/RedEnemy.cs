using System;
using System.Collections;
using System.Threading;
using PingPong.Core;
using PingPong.Utils;
using UnityEngine;

namespace PingPong.Enemies
{
    public class RedEnemy : Enemy
    {
        public RedEnemy(RedEnemyView enemyView, CtxContainer ctxContainer, CancellationToken ct) :
            base(ctxContainer, ProjectileTypes.Red, ct)
        {
            EnemyView = enemyView;
            HP = 100;
        }

        protected override void Flow()
        {
            //HorizonRotation();
        }
    }
}