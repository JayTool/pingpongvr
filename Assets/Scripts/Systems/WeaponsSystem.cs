using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PingPong.Core;
using PingPong.Weapons;
using UnityEngine;
using WeirdFactory.Utils;

namespace PingPong.Systems
{
    public class WeaponsSystem
    {
        private CtxContainer _ctxContainer;
        private List<StationaryWeapon> _spawnedStWeapList;

        public WeaponsSystem (CtxContainer ctxContainer)
        {
            _ctxContainer = ctxContainer;
            _spawnedStWeapList = new List<StationaryWeapon>();
        }
        
        public async void Run(IScope scope, CancellationToken ct)
        {
            using (scope.SubScope(out var innerScope))
            {
                var innerCt = new CancellationTokenSource();
                innerScope.Subscribe(Clean);

                CreateCannon_TEST(scope ,innerCt.Token);
                while (true)
                {
                    if (ct.IsCancellationRequested)
                        break;
                    await Task.Yield();
                }

                void Clean()
                {
                    innerCt.Cancel();
                }
            }
        }

        public void CreateCannon_TEST(IScope scope,CancellationToken ct)
        {
            var weaponsMap = _ctxContainer.MapsContainer.WeaponsPrefabsMap;
            var prefab = weaponsMap.StationaryPrefabs[WeaponsType.SimpleCannon];
            var cannon = GameObject.Instantiate(prefab, new Vector3(0.5f, 0.5f, 0.5f),Quaternion.identity);
            var view = cannon.GetComponent<StationaryWeaponView>();
            
            
            var stWeap = new StationaryWeapon(view, _ctxContainer);
            stWeap.Run(scope, ct);
            
            _spawnedStWeapList.Add(stWeap);
            
        }
    }
}