using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Unity.XR.Oculus;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.XR;
using WeirdFactory.Utils;

namespace PingPong.Core
{
    public class ControllerInput
    {
        public Action<GameObject> ObjectSelectAct;
        public Action OnInputDown;
        public Action OnInputUp;
        
        public Action PrimBtnTouchEvt;
        public Action SecBtnTouchEvt;
        public Action PrimBtnPressEvt;
        public Action SecBtnPressEvt;
        public Action GripBtnPressEvt;
        public Action TriggerBtnPressEvt;
        
        public Transform RaycastedTf => _raycastedTf;
        public Vector2 Trigger2dVec => _trigger2dVec;
        
        public bool PrimBtnPress => _primBtnPress;
        public bool SecBtnPress => _secBtnPress;
        public bool PrimBtnTouch => _primBtnTouch;
        public bool SecBtnTouch => _secBtnTouch;
        public bool GripBtnPress => _gripBtnPress;
        public bool TriggerBtnPress => _triggerBtnPress;
        
        private Transform _raycastedTf;
        private const float SELECT_DISTANCE = 4f;
        
        private Vector2 _trigger2dVec;
        private bool _primBtnTouch;
        private bool _secBtnTouch;
        private bool _primBtnPress;
        private bool _secBtnPress;
        private bool _gripBtnPress;
        private bool _triggerBtnPress;
        
        private bool _primBtnTouchSaved;
        private bool _secBtnTouchSaved;
        private bool _primBtnPressSaved;
        private bool _secBtnPressSaved;
        private bool _gripBtnPressSaved;
        private bool _triggerBtnPressSaved;
        
        private InputDevice _controller;
        private InputDeviceCharacteristics _characteristics;

        public ControllerInput(InputDeviceCharacteristics characteristics)
        {
            _characteristics = characteristics;
        }

        public async void Run(IScope scope, CancellationToken ct)
        {
            using (scope.SubScope(out var innerScope))
            {
                var innerCt = new CancellationTokenSource();
                innerScope.Subscribe(Clean);
                
                VR_Init();
                while (!ct.IsCancellationRequested)
                {
                    SetDevicesValues();
                    await Task.Yield();
                }
                
                void Clean()
                {
                    _raycastedTf = null;
                    innerCt.Cancel();
                }
            }
        }

        private void VR_Init()
        {
            var devices = new List<InputDevice>();
            
            InputDevices.GetDevicesWithCharacteristics(_characteristics, devices);
            Assert.IsTrue(devices.Count > 0);
            _controller = devices[0];
        }

        private void SetDevicesValues()
        {
            _controller.TryGetFeatureValue(CommonUsages.primary2DAxis, out _trigger2dVec);
            _controller.TryGetFeatureValue(CommonUsages.primaryTouch, out _primBtnTouch);
            _controller.TryGetFeatureValue(CommonUsages.secondaryTouch, out _secBtnTouch);
            _controller.TryGetFeatureValue(CommonUsages.primaryButton, out _primBtnPress);
            _controller.TryGetFeatureValue(CommonUsages.secondaryButton, out _secBtnPress);
            _controller.TryGetFeatureValue(CommonUsages.gripButton, out _gripBtnPress);
            _controller.TryGetFeatureValue(CommonUsages.triggerButton, out _triggerBtnPress);
        }

        private void EventsHandler()
        {
        
        /*
        private bool _primBtnTouchSaved;
        private bool _secBtnTouchSaved;
        private bool _primBtnPressSaved;
        private bool _secBtnPressSaved;
        private bool _gripBtnPressSaved;
        private bool _triggerBtnPressSaved;
        */
            if (_primBtnTouchSaved != _primBtnTouch)
            {
                PrimBtnTouchEvt?.Invoke();
                _primBtnTouchSaved = _primBtnTouch;
            }
            
            if (_secBtnTouchSaved != _secBtnTouch)
            {
                SecBtnTouchEvt?.Invoke();
                _secBtnTouchSaved = _secBtnTouch;
            }
            
            if (_primBtnPressSaved != _primBtnPress)
            {
                PrimBtnPressEvt?.Invoke();
                _primBtnPressSaved = _primBtnPress;
            }
            
            if (_secBtnPressSaved != _secBtnPress)
            {
                SecBtnPressEvt?.Invoke();
                _secBtnPressSaved = _secBtnPress;
            }
            
            if (_gripBtnPressSaved != _gripBtnPress)
            {
                GripBtnPressEvt?.Invoke();
                _gripBtnPressSaved = _gripBtnPress;
            }
            
            if (_triggerBtnPressSaved != _triggerBtnPress)
            {
                TriggerBtnPressEvt?.Invoke();
                _triggerBtnPressSaved = _triggerBtnPress;
            }
        }
    }
}