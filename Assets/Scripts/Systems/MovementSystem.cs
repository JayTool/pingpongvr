using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using PingPong.Core;
using UnityEngine;
using UnityEngine.InputSystem;
using WeirdFactory.Utils;

namespace PingPong.Systems
{
    public class MovementSystem
    {
        public CtxContainer _ctxContainer;
        private bool _holdCameraRot;

        public MovementSystem (CtxContainer ctxContainer)
        {
            _ctxContainer = ctxContainer;
        }

        public async void Run(IScope scope, CancellationToken ct)
        {
            using (scope.SubScope(out var innerScope))
            {
                var innerCt = new CancellationTokenSource();
                innerScope.Subscribe(Clean);


                while (!ct.IsCancellationRequested)
                {
//                    if (Input.GetKeyDown(KeyCode.H))
//                        _holdCameraRot = !_holdCameraRot;
                    await Task.Yield();

                    if (!_holdCameraRot)
                    {
                        RotationLogic();
                        //WalkingLogic();
                    }
                    
                    HandMoving();
                }
                
                void Clean()
                {
                    innerCt.Cancel();
                }
            }
        }

        private void RotationLogic()
        {
            #if EDITOR_MODE
            float rotateHorizontal = Input.GetAxis("Mouse X");
            float rotateVertical = Input.GetAxis("Mouse Y");
            
            // Player rotation
            var playerTf = _ctxContainer.PlayerView.PlayerRb.transform;
            var newPlayerEu = new Vector3( rotateHorizontal, 0f, 0f);
            playerTf.rotation *= Quaternion.Euler(0f,newPlayerEu.x,0f);

            // Camera rotation
            var cameraObj = _ctxContainer.CameraRoot;
            var playerForw = playerTf.forward;
            var angle = Vector3.SignedAngle(cameraObj.forward, playerForw, Vector3.right);
            //var newCamEu = new Vector3(cameraEu.x - rotateVertical, cameraEu.y + rotateHorizontal, 0f);
            var newCamEu = new Vector3(-rotateVertical, 0f, 0f);

            if ((angle < 85 && rotateVertical > 0) || (angle > -85 && rotateVertical < 0))
                cameraObj.transform.localRotation *= Quaternion.Euler(newCamEu.x, 0f, 0f);
            #endif

            var rotEu = _ctxContainer.CameraTf.rotation.eulerAngles;
            _ctxContainer.PlayerView.transform.rotation = Quaternion.Euler(0f,rotEu.y, 0f);
        }

        private void WalkingLogic()
        {
            /*
            float forwardAcc = 0f;
            float sideAcc = 0f;
            float multiplier = 1000f;

            float maxForwSpeed = 3f;
            float maxSideSpeed = 3f;
            var walkVec = _ctxContainer.Starter.ControllerInput.Trigger2dVec;

            forwardAcc = walkVec.y * multiplier;
            sideAcc = walkVec.x * multiplier;

            var playerRb = _ctxContainer.PlayerView.PlayerRb;
            var curVel = playerRb.velocity;

            var forwProj = Vector3.Project(curVel, playerRb.transform.forward);
            var rightProj = Vector3.Project(curVel, playerRb.transform.right);

            if (forwProj.sqrMagnitude > maxForwSpeed * maxForwSpeed)
                forwardAcc = 0;
            
            if (rightProj.sqrMagnitude > maxSideSpeed * maxSideSpeed)
                sideAcc = 0;
            playerRb.AddForce(playerRb.transform.forward * forwardAcc + playerRb.transform.right * sideAcc);

            var camRootPos = _ctxContainer.CameraRoot.transform.position;
            _ctxContainer.CameraRoot.transform.position = _ctxContainer.PlayerView.HeadPointTf.position;
            */
        }

        private void HandMoving()
        {
                var rightHandView = _ctxContainer.PlayerView.RightHandView;
                var prevPos = rightHandView.PrevPos;
                var currentPos = rightHandView.transform.position;
                var vel = (currentPos - prevPos).magnitude / Time.deltaTime;
                rightHandView.Velocity = vel;
                rightHandView.PrevPos = currentPos;
        }
    }
}