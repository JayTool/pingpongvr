using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PingPong.Core;
using UnityEngine;
using UnityEngine.Assertions;
using WeirdFactory.Utils;
using Random = UnityEngine.Random;
using PingPong.Views;

namespace PingPong.Enemies
{
    public class AimsManager
    {
        private CtxContainer _ctxContainer;
        private List<AimView> _aimViews;

        public List<AimView> AimViews => _aimViews;

        public AimsManager(CtxContainer ctxContainer)
        {
            _ctxContainer = ctxContainer;
            _aimViews = new List<AimView>();
        }

        public async void Run(IScope scope, CancellationToken ct)
        {
            using (scope.SubScope(out var innerScope))
            {
                var innerCt = new CancellationTokenSource();
                innerScope.Subscribe(Clean);
                
                while (!ct.IsCancellationRequested)
                    await Task.Yield();
                
                void Clean()
                {
                    innerCt.Cancel();
                }
            }
        }

        public void InstantiateTestAim(AimType aimType)
        {
            var testX = Random.Range(-30f, 30f);
            var testY = Random.Range(5f, 20f);
            var testZ = 90f;//Random.Range(20f, 40f);
            var testPos = new Vector3(testX, testY, testZ);
            _ctxContainer.Starter.AimsManager.InstantiateAim(aimType, testPos, Quaternion.identity);
        }

        public void InstantiateAim(AimType aimType, Vector3 spawnPos, Quaternion rot)
        {
            if (aimType == AimType.Default)
            {
                var aimPrefab = _ctxContainer.MapsContainer.ObjectsPrefabsMap.AimPrefabs[AimType.Default];
                var aimContainer = _ctxContainer.AimContainer;
                var aimGo = GameObject.Instantiate(aimPrefab, aimContainer);
                var aimView = aimGo.GetComponent<AimView>();
                
                aimView.CollisionsObserver.OnCollisionEnterEvt += (c) =>
                {
                    c.gameObject.SetActive(false);
                    
                    var fx = GameObject.Instantiate(_ctxContainer.PlayerView.StrongStrikeFx);
                    fx.transform.position = aimView.transform.position;
                };
                /*
                aimView.collisionsSphereCastObserver.OnCollisionEnterEvt += (c) =>
                {
                    Debug.Log("Hit aim!"); 
                    c.gameObject.SetActive(false);
                    
                    var fx = GameObject.Instantiate(_ctxContainer.PlayerView.StrongStrikeFx);
                    fx.transform.position = aimView.transform.position;
                };
                */
                _aimViews.Add(aimView);

                aimGo.transform.localScale = Vector3.one * 1f;
                aimGo.transform.localPosition = spawnPos;
                aimGo.transform.rotation = rot;
            }
            else
            {
                Debug.LogError($"There is no aim type {aimType}");
                return;
            }
        }

        public void HitAim(AimView hitAimView)
        {
            foreach (var aimView in AimViews)
            {
                if (hitAimView == aimView)
                {
                    AimViews.Remove(aimView);
                    
                    var fx = GameObject.Instantiate(_ctxContainer.PlayerView.StrongStrikeFx);
                    fx.transform.position = aimView.transform.position;
                    GameObject.Destroy(aimView.gameObject);

                    InstantiateTestAim(AimType.Default);
                    return;
                }
            }
        }
    }
}