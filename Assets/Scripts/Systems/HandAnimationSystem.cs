using System.Threading;
using System.Threading.Tasks;
using PingPong.Core;
using PingPong.Views;
using UnityEngine;
using WeirdFactory.Utils;

namespace PingPong.Systems
{
    public class HandAnimationSystem
    {
        private ControllerInput _controllerInput;
        private HandView _view;

        public HandAnimationSystem(ControllerInput controllerInput, HandView view)
        {
            _controllerInput = controllerInput;
            _view = view;
        }

        public async void Run(IScope scope, CancellationToken ct)
        {
            //var innerCt = new CancellationTokenSource();

            using (scope.SubScope(out var innerScope))
            {
                
                innerScope.Subscribe(Clean);

                while (!ct.IsCancellationRequested)
                {
                    AnimationFlow();
                    await Task.Yield();
                }
            }

            void Clean()
            {
                //innerCt.Cancel();
            }
        }

        private void AnimationFlow()
        {
            var anim = _view.Animator;

            if (anim == null)
                return;
            
            var primBtnTouch = _controllerInput.PrimBtnTouch;
            var triggerPress = _controllerInput.TriggerBtnPress;
            var gripPress = _controllerInput.GripBtnPress;
            
            var posX = -1f;
            var posY = -1f;

            if (primBtnTouch)
            {
                posX = -1f;
                posY = 1f;
                
                if (gripPress || triggerPress)
                {
                    posY = -1f;
                    posX = 1f;
                }
            }
            else
            {
                if (gripPress)
                {
                    posX = 1f;
                    posY = 1f;
                }
            }

            anim.SetFloat("XPos", posX);
            anim.SetFloat("YPos", posY);
        }
    }
}