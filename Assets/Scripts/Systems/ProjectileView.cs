using System.Collections.Generic;
using PingPong.Enemies;
using UnityEngine;

namespace PingPong.Core
{
    public class ProjectileView : MonoBehaviour
    {
        public ObjectPoolElemReleaser Releaser;
        public Rigidbody Rigidbody;
        public Collider Collider;
        public List<ProjectileTypes> HitFlags;
    }
}