using System;
using PingPong.Core;
using PingPong.Enemies;
using States;
using UnityEngine;
using Random = UnityEngine.Random;

namespace States.BlueShooter
{
    public class AimAndShootState_BlueShooter : EnemyState
    {
        private BlueEnemy Enemy;
        private Transform _CannonTf;
        private Vector3 _targetPos;
        private float _changeTargetTimer;
        private float _rechargeTimer;

        public AimAndShootState_BlueShooter(BlueEnemy enemy)
        {
            Enemy = enemy;
            Enemy.CtxContainer.GizmosVectors.Add((Vector3.zero,Vector3.zero));
            Enemy.CtxContainer.GizmosVectors.Add((Vector3.zero,Vector3.zero));
            _CannonTf = ((BlueEnemyView) Enemy.EnemyView).CannonTf;
        }

        public override void Update()
        {
            TimersUpdate();
            MovingToPos();
            UpdateTarget();
            
            if (HorizonRotation())
                return;

            if (VerticalRotation())
                return;
            
            if (ShotProcess())
                return;
        }
        
        private bool HorizonRotation()
        {
            var shooterView = Enemy.EnemyView;
            var ctxContainer = Enemy.CtxContainer;
            var horThreshold = BlueEnemy.BODY_HORIZONTAL_THRESHOLD;
            var horSpeed = BlueEnemy.BODY_HORIZONTAL_SPEED;
            
            var forwardPr = Vector3.ProjectOnPlane(shooterView.transform.forward, Vector3.up);
            var playerPos = ctxContainer.PlayerView.transform.position;
            var shooterViewPosPr = Vector3.ProjectOnPlane(shooterView.transform.position, Vector3.up);
            var shooterPlayerVecPr = Vector3.ProjectOnPlane(playerPos - shooterViewPosPr, Vector3.up);
            var horVec = Vector3.SignedAngle(shooterPlayerVecPr, forwardPr, Vector3.up);

            Enemy.CtxContainer.GizmosVectors[0] = (shooterViewPosPr, forwardPr);
            Enemy.CtxContainer.GizmosVectors[1] = (shooterViewPosPr, shooterPlayerVecPr);
            if (Math.Abs(horVec) > horThreshold)
            {
                var sign = Math.Sign(horVec);
                shooterView.transform.Rotate(Vector3.up, -sign * horSpeed);
                return true;
            }

            return false;
        }
        
        private bool VerticalRotation()
        {
            var shooterView = Enemy.EnemyView;
            var ctxContainer = Enemy.CtxContainer;
            var verThreshold = BlueEnemy.SHOOTER_VERTICAL_THRESHOLD;
            var speed = BlueEnemy.BODY_VERTICAL_SPEED;
            
            var forward = shooterView.ShotPoints[0].transform.forward;
            var playerPos = ctxContainer.PlayerView.transform.position;
            var shooterPlayerVec = playerPos - shooterView.ShotPoints[0].transform.position + Vector3.up * 1f;
            var verVec = Vector3.SignedAngle(shooterPlayerVec.normalized, forward, shooterView.ShotPoints[0].transform.right);
            
            if (Math.Abs(verVec) > verThreshold)
            {
                var sign = Math.Sign(verVec);
                _CannonTf.transform.rotation *= Quaternion.Euler(-sign * speed, 0f, 0f);
                return true;
            }

            return false;
        }
        
        private bool MovingToPos()
        {
            var shooterView = Enemy.EnemyView;

            var moveVec = _targetPos - shooterView.transform.position;

            if (moveVec.sqrMagnitude > BlueEnemy.TARGET_STOP_DISTANCE)
            {
                shooterView.transform.position += moveVec.normalized * BlueEnemy.MOVING_SPEED;
                return true;
            }
            
            return false;
        }

        private void UpdateTarget()
        {
            if (_changeTargetTimer < 0f)
            {
                _changeTargetTimer = 5f;

                var xPos = Random.Range(10, 25f);
                var zPos = Random.Range(10, 25f);

                var sign = Random.Range(0, 2);

                if (sign == 1)
                    xPos *= -1f;
                
                sign = Random.Range(0, 2);

                if (sign == 1)
                    zPos *= -1f;
                
                
                _targetPos = new Vector3(xPos, 0.5f, zPos);
            }

            
        }

        private void TimersUpdate()
        {
            if (_changeTargetTimer >= 0)
                _changeTargetTimer -= Time.deltaTime;

            if (_rechargeTimer >= 0)
                _rechargeTimer -= Time.deltaTime;
        }

        private bool ShotProcess()
        {
            if (_rechargeTimer < 0)
            {
                _rechargeTimer = BlueEnemy.SHOT_RECHARGE_TIME;
                //TestAction?.Invoke();
                return true;
            }

            return false;
        }
    }
}