using PingPong.Enemies;

namespace States
{
    public abstract class EnemyState
    {
        public Enemy Enemy;
        public bool MovingStateTrigger;
        public bool ShootStateTrigger;

        public abstract void Update();

        public virtual void EnterState()
        {
            
        }
        
        public virtual void ExitState()
        {
            
        }
    }
}