using System;
using PingPong.Enemies;
using UnityEngine;

namespace States.AngryWalle
{
    public class AngryWalle_AimingState : EnemyState
    {
        public bool Aimed;
        public Action<bool> OnAimedChange;
        public Transform TargetTf;

        private bool _savedAimedVal;
        private bool _forceAimChange;
        private AngryWalleEnemy _walleEnemy;
        private const float ROTATION_SPEED = 0.05f;
        private const float CRIT_ANGLE_CANNONS_ROT = 5f;


        public AngryWalle_AimingState(AngryWalleEnemy owner, Transform targetTf = null)
        {
            Enemy = owner;
            _walleEnemy = owner;
            TargetTf = targetTf;
        }

        public override void Update()
        {
            // Body aiming
            var view = _walleEnemy.WalleView;
            var bodyTf = view.BodyTf;
            var bodyForw = bodyTf.forward;
            Vector3 toTarVector = bodyForw;

            if (TargetTf != null)
                toTarVector = TargetTf.position - view.BodyTf.position;
            else
                toTarVector = view.transform.forward;

            var targetRot = Quaternion.LookRotation(toTarVector);
            bodyTf.rotation = Quaternion.Slerp(bodyTf.rotation, targetRot, ROTATION_SPEED);

            var angle = Vector3.Angle(bodyForw, toTarVector);
            var cannonsList = view.CannonsTfList;
            var cannonsReadyList = new bool [cannonsList.Count];

            if (angle < CRIT_ANGLE_CANNONS_ROT)
            {
                for (var i = 0; i < cannonsList.Count; i++)
                {
                    Quaternion targetCannonsRot;
                    if (TargetTf == null)
                    {
                        targetCannonsRot = Quaternion.Euler(Vector3.zero);
                    }
                    else
                    {
                        var distanceToPlayer = Vector3.Distance(view.transform.position, TargetTf.position);
                        var minAngle = 0f;
                        var maxAngle = 45f;
                        var val = minAngle / distanceToPlayer + distanceToPlayer * maxAngle / 50f;
                        var resultAngle = Mathf.Clamp(val, minAngle, maxAngle);
                        var resultVec = new Vector3(-resultAngle, 0f, 0f);

                        targetCannonsRot = Quaternion.Euler(resultVec);

                        var dir = TargetTf.position - cannonsList[i].position;
                        var forw = _walleEnemy.WalleView.BodyTf.forward;
                        var angleOffs = Vector3.SignedAngle(forw, dir, Vector3.up);
                        targetCannonsRot *= Quaternion.Euler(0f, angleOffs, 0f);
                    }

                    cannonsList[i].localRotation = Quaternion.Slerp(cannonsList[i].localRotation, targetCannonsRot,
                        ROTATION_SPEED);
                    
                    var xRot = cannonsList[i].localRotation.eulerAngles.x;
                    var tarRot = targetCannonsRot.eulerAngles.x;
                    var delta = Math.Abs(xRot - tarRot);

                    if (delta < 5f)
                        cannonsReadyList[i] = true;
                    else
                        cannonsReadyList[i] = false;
                }

                Aimed = false;

                foreach (var isReady in cannonsReadyList)
                {
                    if (isReady)
                    {
                        Aimed = true;
                        break;
                    }
                }
            }
            
            if (_savedAimedVal != Aimed || _forceAimChange)
            {
                _savedAimedVal = Aimed;
                OnAimedChange?.Invoke(Aimed);
                _forceAimChange = false;
            }
        }

        public void SetTargetTf(Transform targetTf)
        {
            TargetTf = targetTf;
            Aimed = false;
            _forceAimChange = true;
        }

        public void RemoveTargetTf()
        {
            TargetTf = null;
            Aimed = false;
            _forceAimChange = true;
        }
    }
}