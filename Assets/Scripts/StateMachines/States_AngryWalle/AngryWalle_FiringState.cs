using System;
using PingPong.Core;
using PingPong.Enemies;
using PingPong.Utils;
using UnityEngine;

namespace States.AngryWalle
{
    public class AngryWalle_FiringState : EnemyState
    {
        public Action SingleShotAct;
        private AngryWalleEnemy _walleEnemy;
        private CtxContainer _ctxContainer;
        private int _fireCanId;
        
        public AngryWalle_FiringState(AngryWalleEnemy owner)
        {
            Enemy = owner;
            _walleEnemy = owner;
            _ctxContainer = owner.CtxContainer;
        }
        
        public override void Update()
        {
            if (!_walleEnemy.IsRecharging && !_walleEnemy.IsCooldowning)
            {
                var shotsPts = _walleEnemy.EnemyView.ShotPoints;
                var canNum = shotsPts.Count;
                var cannon = shotsPts[_fireCanId];
                
                Shot(cannon, 2f);
                _fireCanId++;

                if (_fireCanId >= canNum)
                    _fireCanId = 0;
                
                SingleShotAct?.Invoke();
            }
        }
        
        private Projectile GetProjectile()
        {
            var pools = _ctxContainer.Starter.ProjectileManager.Pools;
            return pools.GetElement(_walleEnemy.ProjectileTypes);
        }
        
        private void Shot(Transform shotPoint, float impulseVal)
        {
            var projectile = GetProjectile();
            projectile.ProjectileView.Rigidbody = projectile.ProjectileView.GetComponent<Rigidbody>();
            var projectileTf = projectile.ProjectileView.transform;
            projectileTf.position = shotPoint.position;
            projectileTf.rotation = shotPoint.rotation;
            projectile.SourceEnemyTf = _walleEnemy.EnemyView.transform;
            _ctxContainer.Starter.ProjectileManager.AddProjectile(projectile);

            projectile.ProjectileView.Rigidbody.AddForce(shotPoint.forward * impulseVal,
                ForceMode.Impulse);
        }
    }
}