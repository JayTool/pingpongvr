using System;
using PingPong.Core;
using PingPong.Enemies;
using States;
using UnityEngine;

namespace States.AngryWalle
{
    public class AngryWalle_MovingState : EnemyState
    {
        public Action OnTargetReached;
        public bool Reached;

        private AngryWalleEnemy _walleEnemy;
        private Vector3 _targetPos;

        private float REACH_DISTANCE_SQR = 5f;

        public AngryWalle_MovingState(AngryWalleEnemy owner, Vector3 targetPos)
        {
            Enemy = owner;
            _walleEnemy = owner;
            _targetPos = targetPos;
        }

        public override void Update()
        {
            if (Reached)
                return;

            var view = _walleEnemy.WalleView;
            var enemyForw = view.transform.forward;
            var rigidbody = view.Rigidbody;
            var toTarVector = _targetPos - view.transform.position;
            var targetRot = Quaternion.LookRotation(toTarVector);

            view.transform.rotation = Quaternion.Slerp(view.transform.rotation, targetRot, _walleEnemy.RotationSpeed);

            var maxForwSpeed = _walleEnemy.MaxForwardSpeed;
            if (rigidbody.velocity.sqrMagnitude <= maxForwSpeed * maxForwSpeed)
                rigidbody.AddForce(enemyForw * 15f, ForceMode.Force);

            Reached = toTarVector.sqrMagnitude <= REACH_DISTANCE_SQR;
            
            if (Reached)
                OnTargetReached?.Invoke();
        }

        public void SetNewTarget(Vector3 newTar)
        {
            _targetPos = newTar;
            Reached = false;
        }
    }
}