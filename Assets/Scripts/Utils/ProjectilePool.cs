using System.Collections.Generic;
using PingPong.Core;
using PingPong.Enemies;
using PingPong.Systems;
using UnityEngine;
using UnityEngine.Assertions;

namespace PingPong.Utils
{
    public class ProjectilePool <T> where T : Projectile, new()
    {
        private GameObject _prefab;
        private int _poolCount;
        private Stack<T> _objectStack;
        private GameObject _objectRoot;
        private Transform _gameRootTf;
        
        public ProjectilePool (GameObject prefab, int initPoolCount, Transform gameRootTf)
        {
            _prefab = prefab;
            _poolCount = initPoolCount;
            _objectStack = new Stack<T>();
            _gameRootTf = gameRootTf;
            
            InitiatePool();
        }

        private void InitiatePool()
        {
            _objectRoot = new GameObject($"Object pool ({_prefab.gameObject.name})");
            _objectRoot.gameObject.transform.parent = _gameRootTf;
            _objectRoot.gameObject.transform.localScale = Vector3.one;
            _objectRoot.gameObject.transform.localPosition = Vector3.zero;

            for (int i = 0; i < _poolCount; i++)
            {
                GameObject obj = GameObject.Instantiate(_prefab, _objectRoot.transform);
                obj.transform.localPosition = Vector3.zero;
                obj.gameObject.SetActive(false);
                var releaser = obj.AddComponent<ObjectPoolElemReleaser>();
                releaser.OnDisableAct += CheckElemInStack;
                var newProj = new T();
                var view = obj.GetComponent<ProjectileView>();
                view.Releaser = releaser;
                newProj.ProjectileView = view;
                
                _objectStack.Push(newProj);
            }
        }

        public T GetElement()
        {
            Assert.IsTrue(_objectStack.Count > 0);
            var elem = _objectStack.Pop();
            elem.ProjectileView.gameObject.SetActive(true);
            return elem;
        }

        public void ReleaseElement(T elem)
        {
            elem.ProjectileView.gameObject.SetActive(false);
        }

        public void ReleasePool()
        {
            var stackCount = _objectStack.Count;
            for (int i = 0; i < stackCount; i++)
            {
                var elem = _objectStack.Pop();
                var releaser = elem.ProjectileView.GetComponent<ObjectPoolElemReleaser>();
                releaser.OnDisableAct -= CheckElemInStack;
            }
        }

        private void CheckElemInStack(GameObject elemGo)
        {
            var newProj = new T();
            var view = elemGo.GetComponent<ProjectileView>();
            newProj.ProjectileView = view;

            foreach (var elem in _objectStack)
            {
                if (elem.ProjectileView == view)
                    return;
            }
            
            _objectStack.Push(newProj);
        }
    }
}