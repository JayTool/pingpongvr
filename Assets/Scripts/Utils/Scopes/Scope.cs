﻿using System;

namespace WeirdFactory.Utils
{
    public static class Scope
    {
        public static IDisposable New(out IScope scope)
        {
            var subject = new ScopeStack();
            scope = subject;
            return subject;
        }

        public static IDisposable SubScope(this IScope outer, out IScope result)
        {
            var disposable = Scope.New(out result);
            Action dispose = disposable.Dispose;
            result.Subscribe(Remove);
            outer.Subscribe(dispose);
            return disposable;

            void Remove()
            {
                if (!outer.Disposing)
                    outer.Unsubscribe(dispose);
            }
        }
    }
}