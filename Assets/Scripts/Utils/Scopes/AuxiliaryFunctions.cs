using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace WeirdFactory.Utils
{
    public static class AuxiliaryFunctions
    {
        public static async Task TimerDelay(float time, CancellationToken ct)
        {
            var timer = 0f;
            while(timer < time && !ct.IsCancellationRequested)
            {
                timer += Time.deltaTime;
                await Task.Yield();
            }
        }
    }
}
