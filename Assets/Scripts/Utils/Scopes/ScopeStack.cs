using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace WeirdFactory.Utils
{
    class ScopeStack : IDisposable, IScope
    {
        public bool Disposing { get; private set; }
        public bool Disposed { get; private set; }
        LinkedList<Action> _stack;

        public ScopeStack()
        {
            Disposed = false;
            Disposing = false;
            _stack = new LinkedList<Action>();
        }

        public void Dispose()
        {
            if (Disposed) return;

            if (Disposing)
            {
//                Debug.LogError("Self-looping dispose call");
                return;
            }

            Disposing = true;
            Action cur = null;
            while (TryDequeue(ref cur))
            {
                Assert.IsFalse(Disposed);
                cur.Invoke();
            }

            _stack.Clear();
            _stack = null;
            Disposed = true;

        }

        bool TryDequeue(ref Action value)
        {
            if (_stack.Count == 0) return false;

            value = _stack.Last.Value;
            _stack.RemoveLast();
            return true;
        }

        public void Subscribe(Action dispose)
        {
            if (Disposed || Disposing)
            {
                dispose.Invoke();
                return;
            }

            _stack.AddLast(dispose);
        }

        public void Unsubscribe(Action dispose)
        {
            if (Disposed || Disposing)
            {
                Debug.LogError("Cannot unsubscribe during or after disposal");
                return;
            }

            var any = _stack.Remove(dispose);
            Assert.IsTrue(any, "Delegate not found: make sure it's the same which was passed to OnDispose");
        }
    }
}