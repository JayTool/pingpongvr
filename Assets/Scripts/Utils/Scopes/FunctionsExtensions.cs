namespace WeirdFactory.Utils
{
    public static class FunctionsExtensions
    {
        public static float TrimValue(this float val, float min, float max)
        {
            if (val < min)
                val = min;

            if (val > max)
                val = max;

            return val;
        }
        
        public static int TrimValue(this int val, int min, int max)
        {
            if (val < min)
                val = min;

            if (val > max)
                val = max;

            return val;
        }
    }
}