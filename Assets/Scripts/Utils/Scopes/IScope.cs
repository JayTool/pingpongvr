﻿using System;

namespace WeirdFactory.Utils
{
public interface IScope
{
    bool Disposing { get; }
    bool Disposed { get; }
    void Subscribe(Action dispose);
    void Unsubscribe(Action dispose);
}
}

