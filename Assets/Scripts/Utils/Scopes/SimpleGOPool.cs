using System.Collections.Generic;
using UnityEngine;

namespace WeirdFactory.Utils.Copticool.Utils
{
    public class SimpleGOPool
    {
        private List<GameObject> PoolList;
        private const int BUFFER_SIZE = 25;
        private GameObject _prefab;
        private GameObject _rootObj;

        public SimpleGOPool(GameObject initObj)
        {
            PoolList = new List<GameObject>();
            Init(initObj, BUFFER_SIZE);
        }

        private void Init(GameObject initObj, int bufferCount)
        {
            _prefab = initObj;
            _rootObj = new GameObject($"ObjPool - {initObj.gameObject.name}");
            AddNewElements(initObj, BUFFER_SIZE);
        }

        private void AddNewElements(GameObject prefab, int size)
        {
            for (int i = 0; i < size; i++)
            {
                var newObj = GameObject.Instantiate(prefab, _rootObj.transform);
                newObj.transform.localPosition = Vector3.zero;
                newObj.transform.rotation = Quaternion.identity;
                newObj.SetActive(false);
                PoolList.Add(newObj);
            }
        }

        public GameObject GetOrAdd ()
        {
            foreach (var elem in PoolList)
            {
                if (!elem.gameObject.activeInHierarchy)
                {
                    return elem;
                }
            }
            
            AddNewElements(_prefab, 10);

            return GetOrAdd();
        }

        public void Release(GameObject go)
        {
            go.SetActive(false);
        }
    }
}