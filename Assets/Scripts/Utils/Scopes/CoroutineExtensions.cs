using System.Collections;

namespace WeirdFactory.Utils
{
    public static class CoroutineExtensions
    {
        public static IEnumerator BreakOn(this IEnumerator coroutine, IScope scope)
        {
            while (!scope.Disposing && coroutine.MoveNext())
                yield return coroutine.Current;
        }
    }
    
    namespace Copticool.Utils
    {
    }
}