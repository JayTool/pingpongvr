using System;
using UnityEngine;

namespace PingPong.Core
{
    public class CollisionsObserver : MonoBehaviour
    {
        public event Action <Collision> OnCollisionEnterEvt;
        public event Action <Collision> OnCollisionExitEvt;
        public event Action <Collider> OnTriggerEnterEvt;
        public event Action <Collider> OnTriggerExitEvt;

        private void OnCollisionEnter(Collision other)
        {
            OnCollisionEnterEvt?.Invoke(other);
        }
        
        private void OnCollisionExit(Collision other)
        {
            OnCollisionExitEvt?.Invoke(other);
        }

        private void OnTriggerEnter(Collider other)
        {
            OnTriggerEnterEvt?.Invoke(other);
        }
        
        private void OnTriggerExit(Collider other)
        {
            OnTriggerExitEvt?.Invoke(other);
        }
    }
}