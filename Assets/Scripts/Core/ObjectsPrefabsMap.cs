using System.Collections.Generic;
using PingPong.Enemies;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Utils.Maps
{
    [CreateAssetMenu(menuName = "ViewMaps/ObjectsViewMap")]
    public class ObjectsPrefabsMap : SerializedScriptableObject
    {
        [SerializeField] public Dictionary<AimType, GameObject> AimPrefabs;
    }
}