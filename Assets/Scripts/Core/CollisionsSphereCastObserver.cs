using System;
using UnityEngine;
using WeirdFactory.Utils;

namespace PingPong.Core
{
    public class CollisionsSphereCastObserver : MonoBehaviour
    {
        public event Action <Transform> OnCollisionEvt;
        public LayerMask LayerMask;
        public Transform RayObjectDirTf;

        private const float STEP_DISTANCE = 0.3f;
        private const float Radius = 0.075f;
        private Transform _cachedHitTf;
        private Vector3 _prevPos;
        private float _currentVelocity;

        private Ray _onGizmosRay;
        private float _onGizmosRayLenght;
        private void Update()
        {
            var ray = new Ray(RayObjectDirTf.position, RayObjectDirTf.forward);
            _onGizmosRay = ray;
            var maxDist = STEP_DISTANCE + _currentVelocity * 0.05f;
            maxDist = maxDist.TrimValue(0f, 0.5f);
            _onGizmosRayLenght = maxDist;
            Physics.SphereCast(ray, Radius, out var hit, maxDist, LayerMask);

            if (hit.transform != _cachedHitTf)
            {
                _cachedHitTf = hit.transform;
                
                if (_cachedHitTf != null)
                    CollisionEvent(_cachedHitTf);
            }

            var currentPos = transform.position;
            _currentVelocity = (currentPos - _prevPos).magnitude / Time.deltaTime;
            _prevPos = currentPos;
        }

        private void CollisionEvent(Transform colTf)
        {
            OnCollisionEvt?.Invoke(colTf);
        }

        private void OnDrawGizmos()
        {
            if (_onGizmosRay.origin != Vector3.zero)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawRay(_onGizmosRay.origin, _onGizmosRay.direction.normalized *
                                                    (_onGizmosRayLenght));
                Gizmos.color = Color.green;
                var cross = Vector3.Cross(_onGizmosRay.direction, Vector3.up).normalized;

                for (int i = 0; i < 360; i=i+2)
                {
                    var finalAngle = Quaternion.AngleAxis(i, _onGizmosRay.direction.normalized) * cross;
                    Gizmos.DrawRay(_onGizmosRay.origin + finalAngle * Radius,
                        _onGizmosRay.direction.normalized * (_onGizmosRayLenght));
                }
                
                Gizmos.color = Color.white;
            }
        }
    }
}