using UnityEngine;
using Utils.Maps;

namespace PingPong.Core
{
    public class MapsContainer : MonoBehaviour
    {
        public EnemiesPrefabsMap enemiesPrefabsMap;
        public ObjectsPrefabsMap ObjectsPrefabsMap;
        public ProjectilesPrefabsMap ProjectilesPrefabsMap;
        public WeaponsPrefabsMap WeaponsPrefabsMap;
    }
}