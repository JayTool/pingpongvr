using PingPong.Enemies;
using PingPong.Utils;

namespace PingPong.Core
{
    public class Pools
    {
        private ProjectilePool<RedProjectile> _redProjectile_Pool;
        private ProjectilePool<BlueProjectile> _blueProjectile_Pool;
        private ProjectilePool<CannonballProjectile> _cannonProjectile_Pool;

        public Pools(CtxContainer _ctxContainer)
        {
            var projMap = _ctxContainer.MapsContainer.ProjectilesPrefabsMap.ProjectilesPrefabs;
            
            _redProjectile_Pool = new ProjectilePool<RedProjectile>(projMap[ProjectileTypes.Red],
                500, _ctxContainer.GameRootTf);
            
            _blueProjectile_Pool = new ProjectilePool<BlueProjectile>(projMap[ProjectileTypes.Blue],
                50, _ctxContainer.GameRootTf);
            
            _cannonProjectile_Pool = new ProjectilePool<CannonballProjectile>(projMap[ProjectileTypes.Cannonball],
                50, _ctxContainer.GameRootTf);
        }

        public Projectile GetElement(ProjectileTypes type)
        {
            switch (type)
            {
                case ProjectileTypes.Red:
                    return _redProjectile_Pool.GetElement();
                
                case ProjectileTypes.Blue:
                    return _blueProjectile_Pool.GetElement();
            }

            return _cannonProjectile_Pool.GetElement();
        }
    }
}