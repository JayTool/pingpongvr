using System.Collections.Generic;
using States;
using UnityEngine;
using UnityEngine.Assertions;

namespace PingPong.Core
{
    public class StateMachine
    {
        public List<EnemyState> CurrentStates;

        private bool _updateTrigger;
        
        public StateMachine(EnemyState initStates)
        {
            CurrentStates = new List<EnemyState> {initStates};
            CurrentStates[0].EnterState();
        }

        public void ChangeAllToState(EnemyState newState)
        {
            foreach (var state in CurrentStates)
                state.ExitState();
            
            CurrentStates.Clear();
            CurrentStates.Add(newState);
            CurrentStates[0].EnterState();
        }

        public void AddState(EnemyState newState)
        {
            Debug.LogWarning( $"State already added ({newState})");
            newState.EnterState();
            CurrentStates.Add(newState);
            _updateTrigger = true;
        }

        public void TryRemoveState(EnemyState removingState)
        {
            if (!CurrentStates.Contains(removingState))
                return;
            
            removingState.ExitState();
            CurrentStates.Remove(removingState);
            _updateTrigger = true;
        }

        public void Update()
        {
            foreach (var state in CurrentStates)
            {
                state.Update();

                if (_updateTrigger)
                {
                    _updateTrigger = false;
                    break;
                }
            }
        }
    }
}