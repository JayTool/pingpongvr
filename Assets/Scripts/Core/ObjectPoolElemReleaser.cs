 using System;
using UnityEngine;

namespace PingPong.Core
{
    public class ObjectPoolElemReleaser : MonoBehaviour
    {
        public event Action <GameObject> OnDisableAct;

        private void OnDisable()
        {
            OnDisableAct?.Invoke(gameObject);
        }
    }
}