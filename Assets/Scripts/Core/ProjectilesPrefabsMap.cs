using System.Collections.Generic;
using PingPong.Enemies;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Utils.Maps
{
    [CreateAssetMenu(menuName = "ViewMaps/ProjectilesMap")]
    public class ProjectilesPrefabsMap : SerializedScriptableObject
    {
        [SerializeField] public Dictionary<ProjectileTypes, GameObject> ProjectilesPrefabs;
    }
}