using System.Collections.Generic;
using PingPong.Enemies;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Utils.Maps
{
    [CreateAssetMenu(menuName = "ViewMaps/EnemyViewMap")]
    public class EnemiesPrefabsMap : SerializedScriptableObject
    {
        [SerializeField] public Dictionary<EnemyType, GameObject> EnemiesPrefabs;
    }
}