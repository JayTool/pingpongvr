﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PingPong.Enemies;
using PingPong.Systems;
using Sirenix.Utilities;
using UnityEngine;
using WeirdFactory.Utils;

namespace PingPong.Core
{
    public class Starter : MonoBehaviour
    {
        public CtxContainer CtxContainer;
        public GameFlow GameFlow;
       //public ControllerInput ControllerInput;
        public MovementSystem MovementSystem;
        public EnemiesManager EnemiesManager;
        public ManipulatorsSystem ManipulatorsSystem;
        public ProjectileManager ProjectileManager;
        public AimsManager AimsManager;
        public WeaponsSystem WeaponsSystem;

        private CancellationTokenSource _mainCt;
        private void Start()
        {
            //ControllerInput = new ControllerInput(CtxContainer);
            MovementSystem = new MovementSystem(CtxContainer);
            EnemiesManager = new EnemiesManager(CtxContainer);
            GameFlow = new GameFlow(CtxContainer);
            ManipulatorsSystem = new ManipulatorsSystem(CtxContainer);
            CtxContainer.GizmosVectors = new List<(Vector3, Vector3)>();
            CtxContainer.GizmosPoints = new List<Vector3>();
            ProjectileManager = new ProjectileManager(CtxContainer);
            AimsManager = new AimsManager(CtxContainer);
            WeaponsSystem = new WeaponsSystem(CtxContainer);
            
            StarterRun();
        }

        private async void StarterRun()
        {
            _mainCt = new CancellationTokenSource();
            using (Scope.New(out var mainScope))
            {
                var ct = new CancellationTokenSource();
                var ct_token = ct.Token;
                mainScope.Subscribe(Clean);
                
                //ControllerInput.Run(mainScope, ct_token);
                MovementSystem.Run(mainScope, ct_token);
                ManipulatorsSystem.Run(mainScope, ct_token);
                ProjectileManager.Run(mainScope, ct_token);
                AimsManager.Run(mainScope, ct_token);
                WeaponsSystem.Run(mainScope, ct_token);
                GameFlow.Run(mainScope, ct_token);
                
                while (!_mainCt.Token.IsCancellationRequested)
                    await Task.Yield();

                void Clean()
                {
                    _mainCt.Cancel();
                    ct.Cancel();
                }
            }
        }

        private void OnDrawGizmos()
        {
            var vecGizList = CtxContainer.GizmosVectors;
            var vecGizPointsList = CtxContainer.GizmosPoints;

            if (!vecGizList.IsNullOrEmpty())
                foreach (var vec in vecGizList)
                    Gizmos.DrawLine(vec.origin, (vec.to) * 10f);

            if (!vecGizPointsList.IsNullOrEmpty())
            {
                Gizmos.color = Color.red;
                foreach (var vec in vecGizPointsList)
                    Gizmos.DrawSphere(vec, 0.2f);
            }
        }

        private void OnApplicationQuit()
        {
            _mainCt.Cancel();
        }
    }
}