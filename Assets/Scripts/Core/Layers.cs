using UnityEngine;

namespace PingPong.Core
{
    public static class Layers
    {
        public static int AimMask = 1 << LayerMask.NameToLayer("Aim");
        public static int ProjectileMask = 1 << LayerMask.NameToLayer("Projectile");
        
        public static int ProjectileLayer = LayerMask.NameToLayer("Projectile");
        
        public static string Projectile_Name = LayerMask.LayerToName(11);
    }
}