using System.Collections.Generic;
using PingPong.Systems;
using PingPong.Views;
using UnityEngine;

namespace PingPong.Core
{
    public class CtxContainer : MonoBehaviour
    {
        public CoroutineController CoroutineController;
        public Starter Starter;
        public Transform CameraRoot;
        public Transform CameraTf;
        public Transform GameRootTf;
        public Transform AimContainer;
        public MapsContainer MapsContainer;
        public PlayerView PlayerView;
        public Camera MainCamera;
        public Transform TEST_TF;

        public List<(Vector3 origin, Vector3 to)> GizmosVectors;
        public List<Vector3> GizmosPoints;

        private void Start()
        {
            TEST_TF.rotation = Quaternion.Euler(new Vector3(45f,0f,0f));
        }
    }

    /*
    // Hit objects only included to layermask
    var colLayer = col.gameObject.layer;
    var bitLayer = 1 << colLayer;

    if ((layerMask.value | (bitLayer)) == layerMask.value)
    {
    if (!hitRbList.Contains(col.attachedRigidbody) && (col.gameObject.layer != LayerMask.NameToLayer("Ground")
    && col.gameObject.layer != LayerMask.NameToLayer("Environment")))
    {
    */
}