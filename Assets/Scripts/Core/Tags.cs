namespace PingPong.Core
{
    public static class Tags
    {
        public const string TRANSFORMER = "Transformer";
        public const string FIGURE = "Figure";
        public const string PICK_PLACE = "PickPlace";
        public const string MANIPULATION_BTN = "ManipulationBtn";
    }
}