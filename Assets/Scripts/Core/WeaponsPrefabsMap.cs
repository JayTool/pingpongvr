using System.Collections.Generic;
using PingPong.Enemies;
using PingPong.Weapons;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Utils.Maps
{
    [CreateAssetMenu(menuName = "ViewMaps/WeaponsViewMap")]
    public class WeaponsPrefabsMap : SerializedScriptableObject
    {
        [SerializeField] public Dictionary<WeaponsType, GameObject> StationaryPrefabs;
    }
}