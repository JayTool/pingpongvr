using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using PingPong.Enemies;
using UnityEngine;
using Utils.Maps;
using WeirdFactory.Utils;

namespace PingPong.Core
{
    public class GameFlow
    {
        public CtxContainer _ctxContainer;

        public GameFlow (CtxContainer ctxContainer)
        {
            _ctxContainer = ctxContainer;
        }

        public async void Run(IScope scope, CancellationToken ct)
        {
            using (scope.SubScope(out var innerScope))
            {
                var innerCt = new CancellationTokenSource();

                innerScope.Subscribe(Clean);
                //RunTestShooters(EnemyType.RedAngryWalle ,innerScope, innerCt.Token);
                //_ctxContainer.Starter.AimsManager.InstantiateTestAim(AimType.Default);
                
                while (!ct.IsCancellationRequested)
                    await Task.Yield();
                
                void Clean()
                {
                    innerCt.Cancel();
                }
            }

        }

        private void RunTestShooters(EnemyType enemyType ,IScope scope, CancellationToken ct)
        {
            var testPos1 = new Vector3(0f, 0.5f, 25f);
            var testRot1 = Quaternion.Euler(0f, 180f, 0f);
            _ctxContainer.Starter.EnemiesManager.InstantiateEnemy(enemyType, testPos1, testRot1, scope, ct);
        }
    }
}